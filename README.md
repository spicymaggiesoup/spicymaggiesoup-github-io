yarn init -y
yarn add react react-dom

yarn add -D webpack webpack-cli webpack-dev-server webpack-merge
(webpack-bundle-analyzer 제외)

yarn add -D @babel/cli @babel/core @babel/preset-env @babel/preset-react babel-loader
(@babel/preset-typescript 제외)

yarn add -D core-js css-loader css-minimizer-webpack-plugin html-webpack-plugin mini-css-extract-plugin style-loader sass sass-loader terser-webpack-plugin

yarn add -D @types/react @types/react-dom 
(typescript 제외)