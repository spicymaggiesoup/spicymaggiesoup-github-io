export default [
    {
      id: 1,
      name: 'Bertie Yates',
      age: 29,
      icon: '',
    },
    {
      id: 2,
      name: 'Hester Hogan',
      age: 32,
      icon: '',
    },
    {
      id: 3,
      name: 'Larry Little',
      age: 36,
      icon: '',
    },
    {
      id: 4,
      name: 'Sean Walsh',
      age: 34,
      icon: '',
    },
    {
      id: 5,
      name: 'Lola Gardner',
      age: 29,
      icon: '',
    },
  ];
  